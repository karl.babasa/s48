import React from 'react';
import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
	return(
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Accusantium, tempore, a. Ipsum voluptate cupiditate quibusdam, eaque maiores sapiente, id rem ipsam, est tempora quae debitis quia atque omnis eius voluptatibus!

						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Accusantium, tempore, a. Ipsum voluptate cupiditate quibusdam, eaque maiores sapiente, id rem ipsam, est tempora quae debitis quia atque omnis eius voluptatibus!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Accusantium, tempore, a. Ipsum voluptate cupiditate quibusdam, eaque maiores sapiente, id rem ipsam, est tempora quae debitis quia atque omnis eius voluptatibus!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>



		)
}