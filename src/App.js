import React from 'react';
import './App.css';
import AppNavBar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Error from './pages/Error';
import {Container} from 'react-bootstrap';

//For routes

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

//The Router(BrowserRouter) conponent will enable us to simulate page navigation by syncronizaing he shown content and the shwon URL in the web Browser.

//The Routes(before it is calls Switch)

function App() {
  return (
    <Router>
      <AppNavBar/>
      <Container>
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/courses" element={<Courses/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="*" element={<Error/>} />
        </Routes>
      </Container>
    </Router>
  );
}   

export default App;

//fo adding Javascript(Babel) Linting in sublime text
// ctrl shift P > tpe install > click the Package Control Install > Babel


